Public Class DataForm
    Inherits System.Windows.Forms.Form
    Const MSG_SAVE_BEFORE_CLOSE = "Du har gjort noen endringer, vil du lagre f�r du avslutter?"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents objCaseList As CaseListEdit.CaseList
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents editAutonomyThreshold2 As System.Windows.Forms.TextBox
    Friend WithEvents editDays2 As System.Windows.Forms.TextBox
    Friend WithEvents lblAutonomyThreshold2 As System.Windows.Forms.Label
    Friend WithEvents lblDays2 As System.Windows.Forms.Label
    Friend WithEvents OleDbCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAutonomyThreshold As System.Windows.Forms.Label
    Friend WithEvents editDays As System.Windows.Forms.TextBox
    Friend WithEvents editAutonomyThreshold As System.Windows.Forms.TextBox
    Friend WithEvents lblDays As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents OleDbCommand2 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbCommand3 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbCommand4 As System.Data.OleDb.OleDbCommand
    Friend WithEvents lblKeyWordID As System.Windows.Forms.Label
    Friend WithEvents lblListNumber As System.Windows.Forms.Label
    Friend WithEvents editScanpixID As System.Windows.Forms.TextBox
    Friend WithEvents editDateTimeCreation As System.Windows.Forms.TextBox
    Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    Friend WithEvents btnNavNext As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblKeyWord As System.Windows.Forms.Label
    Friend WithEvents lblScanpixID As System.Windows.Forms.Label
    Friend WithEvents lblDateTimeCreation As System.Windows.Forms.Label
    Friend WithEvents editKeyWordID As System.Windows.Forms.TextBox
    Friend WithEvents editListNumber As System.Windows.Forms.TextBox
    Friend WithEvents editDescription As System.Windows.Forms.TextBox
    Friend WithEvents editKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    Friend WithEvents lblNavLocation As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnAvslutt As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbInsertCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbUpdateCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbDeleteCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents editMaingroup As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(DataForm))
        Me.objCaseList = New CaseListEdit.CaseList()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter()
        Me.OleDbDeleteCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.OleDbInsertCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbUpdateCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.editAutonomyThreshold2 = New System.Windows.Forms.TextBox()
        Me.editDays2 = New System.Windows.Forms.TextBox()
        Me.lblAutonomyThreshold2 = New System.Windows.Forms.Label()
        Me.lblDays2 = New System.Windows.Forms.Label()
        Me.OleDbCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblAutonomyThreshold = New System.Windows.Forms.Label()
        Me.editDays = New System.Windows.Forms.TextBox()
        Me.editAutonomyThreshold = New System.Windows.Forms.TextBox()
        Me.lblDays = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.OleDbCommand2 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbCommand3 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbCommand4 = New System.Data.OleDb.OleDbCommand()
        Me.lblKeyWordID = New System.Windows.Forms.Label()
        Me.lblListNumber = New System.Windows.Forms.Label()
        Me.editScanpixID = New System.Windows.Forms.TextBox()
        Me.editDateTimeCreation = New System.Windows.Forms.TextBox()
        Me.btnNavPrev = New System.Windows.Forms.Button()
        Me.btnNavNext = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblKeyWord = New System.Windows.Forms.Label()
        Me.lblScanpixID = New System.Windows.Forms.Label()
        Me.lblDateTimeCreation = New System.Windows.Forms.Label()
        Me.editKeyWordID = New System.Windows.Forms.TextBox()
        Me.editListNumber = New System.Windows.Forms.TextBox()
        Me.editDescription = New System.Windows.Forms.TextBox()
        Me.editKeyWord = New System.Windows.Forms.TextBox()
        Me.btnNavFirst = New System.Windows.Forms.Button()
        Me.lblNavLocation = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnAvslutt = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.CheckBox2 = New System.Windows.Forms.CheckBox()
        Me.CheckBox3 = New System.Windows.Forms.CheckBox()
        Me.editMaingroup = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.objCaseList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'objCaseList
        '
        Me.objCaseList.DataSetName = "CaseList"
        Me.objCaseList.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.objCaseList.Namespace = "http://www.tempuri.org/CaseList.xsd"
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.DeleteCommand = Me.OleDbDeleteCommand1
        Me.OleDbDataAdapter1.InsertCommand = Me.OleDbInsertCommand1
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "KeyWord", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("KeyWordID", "KeyWordID"), New System.Data.Common.DataColumnMapping("ListNumber", "ListNumber"), New System.Data.Common.DataColumnMapping("Description", "Description"), New System.Data.Common.DataColumnMapping("KeyWord", "KeyWord"), New System.Data.Common.DataColumnMapping("ScanpixID", "ScanpixID"), New System.Data.Common.DataColumnMapping("DateTimeCreation", "DateTimeCreation"), New System.Data.Common.DataColumnMapping("AutonomyThreshold", "AutonomyThreshold"), New System.Data.Common.DataColumnMapping("AutonomyThreshold2", "AutonomyThreshold2"), New System.Data.Common.DataColumnMapping("Days", "Days"), New System.Data.Common.DataColumnMapping("Days2", "Days2"), New System.Data.Common.DataColumnMapping("Maingroup", "Maingroup")})})
        Me.OleDbDataAdapter1.UpdateCommand = Me.OleDbUpdateCommand1
        '
        'OleDbDeleteCommand1
        '
        Me.OleDbDeleteCommand1.CommandText = "DELETE FROM KeyWord WHERE (KeyWordID = ?) AND (AutonomyThreshold = ?) AND (Autono" & _
        "myThreshold2 = ?) AND (DateTimeCreation = ?) AND (Days = ?) AND (Days2 = ?) AND " & _
        "(Description = ?) AND (ListNumber = ?) AND (Maingroup = ? OR ? IS NULL AND Maing" & _
        "roup IS NULL) AND (ScanpixID = ?)"
        Me.OleDbDeleteCommand1.Connection = Me.OleDbConnection1
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Maingroup", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maingroup", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Maingroup1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maingroup", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        '
        'OleDbConnection1
        '
        Me.OleDbConnection1.ConnectionString = "Provider=SQLOLEDB.1;Password=portal;Persist Security Info=True;User ID=sa;Initial" & _
        " Catalog=ntb;Data Source=193.75.33.39;Use Procedure for Prepare=1;Auto Translate" & _
        "=True;Packet Size=4096;Workstation ID=GX240-PORTAL-0;Use Encryption for Data=Fal" & _
        "se;Tag with column collation when possible=False"
        '
        'OleDbInsertCommand1
        '
        Me.OleDbInsertCommand1.CommandText = "INSERT INTO KeyWord(ListNumber, Description, KeyWord, ScanpixID, DateTimeCreation" & _
        ", AutonomyThreshold, AutonomyThreshold2, Days, Days2, Maingroup) VALUES (?, ?, ?" & _
        ", ?, ?, ?, ?, ?, ?, ?); SELECT KeyWordID, ListNumber, Description, KeyWord, Scan" & _
        "pixID, DateTimeCreation, AutonomyThreshold, AutonomyThreshold2, Days, Days2, Mai" & _
        "ngroup FROM KeyWord WHERE (KeyWordID = @@IDENTITY)"
        Me.OleDbInsertCommand1.Connection = Me.OleDbConnection1
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold2"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days", System.Data.OleDb.OleDbType.Integer, 4, "Days"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days2", System.Data.OleDb.OleDbType.Integer, 4, "Days2"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Maingroup", System.Data.OleDb.OleDbType.VarChar, 64, "Maingroup"))
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT KeyWordID, ListNumber, Description, KeyWord, ScanpixID, DateTimeCreation, " & _
        "AutonomyThreshold, AutonomyThreshold2, Days, Days2, Maingroup FROM KeyWord"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbUpdateCommand1
        '
        Me.OleDbUpdateCommand1.CommandText = "UPDATE KeyWord SET ListNumber = ?, Description = ?, KeyWord = ?, ScanpixID = ?, D" & _
        "ateTimeCreation = ?, AutonomyThreshold = ?, AutonomyThreshold2 = ?, Days = ?, Da" & _
        "ys2 = ?, Maingroup = ? WHERE (KeyWordID = ?) AND (AutonomyThreshold = ?) AND (Au" & _
        "tonomyThreshold2 = ?) AND (DateTimeCreation = ?) AND (Days = ?) AND (Days2 = ?) " & _
        "AND (Description = ?) AND (ListNumber = ?) AND (Maingroup = ? OR ? IS NULL AND M" & _
        "aingroup IS NULL) AND (ScanpixID = ?); SELECT KeyWordID, ListNumber, Description" & _
        ", KeyWord, ScanpixID, DateTimeCreation, AutonomyThreshold, AutonomyThreshold2, D" & _
        "ays, Days2, Maingroup FROM KeyWord WHERE (KeyWordID = ?)"
        Me.OleDbUpdateCommand1.Connection = Me.OleDbConnection1
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold2"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days", System.Data.OleDb.OleDbType.Integer, 4, "Days"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days2", System.Data.OleDb.OleDbType.Integer, 4, "Days2"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Maingroup", System.Data.OleDb.OleDbType.VarChar, 64, "Maingroup"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Maingroup", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maingroup", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Maingroup1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Maingroup", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Select_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, "KeyWordID"))
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.AddRange(New System.Windows.Forms.Control() {Me.editAutonomyThreshold2, Me.editDays2, Me.lblAutonomyThreshold2, Me.lblDays2})
        Me.GroupBox2.Location = New System.Drawing.Point(464, 248)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(184, 96)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Bakgrunn - Analyse -Fakta"
        '
        'editAutonomyThreshold2
        '
        Me.editAutonomyThreshold2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.AutonomyThreshold2"))
        Me.editAutonomyThreshold2.Location = New System.Drawing.Point(112, 56)
        Me.editAutonomyThreshold2.Name = "editAutonomyThreshold2"
        Me.editAutonomyThreshold2.Size = New System.Drawing.Size(48, 20)
        Me.editAutonomyThreshold2.TabIndex = 3
        Me.editAutonomyThreshold2.Text = ""
        '
        'editDays2
        '
        Me.editDays2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.Days2"))
        Me.editDays2.Location = New System.Drawing.Point(112, 24)
        Me.editDays2.Name = "editDays2"
        Me.editDays2.Size = New System.Drawing.Size(48, 20)
        Me.editDays2.TabIndex = 1
        Me.editDays2.Text = ""
        '
        'lblAutonomyThreshold2
        '
        Me.lblAutonomyThreshold2.Location = New System.Drawing.Point(16, 56)
        Me.lblAutonomyThreshold2.Name = "lblAutonomyThreshold2"
        Me.lblAutonomyThreshold2.Size = New System.Drawing.Size(80, 23)
        Me.lblAutonomyThreshold2.TabIndex = 2
        Me.lblAutonomyThreshold2.Text = "Relevans %"
        '
        'lblDays2
        '
        Me.lblDays2.Location = New System.Drawing.Point(16, 24)
        Me.lblDays2.Name = "lblDays2"
        Me.lblDays2.Size = New System.Drawing.Size(80, 23)
        Me.lblDays2.TabIndex = 0
        Me.lblDays2.Text = "Antall dager"
        '
        'OleDbCommand1
        '
        Me.OleDbCommand1.CommandText = "SELECT KeyWordID, ListNumber, Description, KeyWord, ScanpixID, DateTimeCreation, " & _
        "AutonomyThreshold, AutonomyThreshold2, Days, Days2 FROM KeyWord"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.lblAutonomyThreshold, Me.editDays, Me.editAutonomyThreshold, Me.lblDays})
        Me.GroupBox1.Location = New System.Drawing.Point(120, 248)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(176, 96)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "L�pende saker"
        '
        'lblAutonomyThreshold
        '
        Me.lblAutonomyThreshold.Location = New System.Drawing.Point(16, 56)
        Me.lblAutonomyThreshold.Name = "lblAutonomyThreshold"
        Me.lblAutonomyThreshold.Size = New System.Drawing.Size(72, 23)
        Me.lblAutonomyThreshold.TabIndex = 2
        Me.lblAutonomyThreshold.Text = "Relevans %"
        '
        'editDays
        '
        Me.editDays.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.Days"))
        Me.editDays.Location = New System.Drawing.Point(104, 24)
        Me.editDays.Name = "editDays"
        Me.editDays.Size = New System.Drawing.Size(48, 20)
        Me.editDays.TabIndex = 1
        Me.editDays.Text = ""
        '
        'editAutonomyThreshold
        '
        Me.editAutonomyThreshold.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.AutonomyThreshold"))
        Me.editAutonomyThreshold.Location = New System.Drawing.Point(104, 56)
        Me.editAutonomyThreshold.Name = "editAutonomyThreshold"
        Me.editAutonomyThreshold.Size = New System.Drawing.Size(48, 20)
        Me.editAutonomyThreshold.TabIndex = 3
        Me.editAutonomyThreshold.Text = ""
        '
        'lblDays
        '
        Me.lblDays.Location = New System.Drawing.Point(16, 24)
        Me.lblDays.Name = "lblDays"
        Me.lblDays.Size = New System.Drawing.Size(72, 23)
        Me.lblDays.TabIndex = 0
        Me.lblDays.Text = "Antall dager"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(376, 392)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.TabIndex = 22
        Me.btnUpdate.Text = "&Lagre"
        '
        'OleDbCommand2
        '
        Me.OleDbCommand2.CommandText = "DELETE FROM KeyWord WHERE (KeyWordID = ?) AND (AutonomyThreshold = ? OR ? IS NULL" & _
        " AND AutonomyThreshold IS NULL) AND (AutonomyThreshold2 = ? OR ? IS NULL AND Aut" & _
        "onomyThreshold2 IS NULL) AND (DateTimeCreation = ? OR ? IS NULL AND DateTimeCrea" & _
        "tion IS NULL) AND (Days = ? OR ? IS NULL AND Days IS NULL) AND (Days2 = ? OR ? I" & _
        "S NULL AND Days2 IS NULL) AND (Description = ? OR ? IS NULL AND Description IS N" & _
        "ULL) AND (ListNumber = ?) AND (ScanpixID = ? OR ? IS NULL AND ScanpixID IS NULL)" & _
        ""
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold21", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation1", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days21", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand2.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID1", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        '
        'OleDbCommand3
        '
        Me.OleDbCommand3.CommandText = "UPDATE KeyWord SET ListNumber = ?, Description = ?, KeyWord = ?, ScanpixID = ?, D" & _
        "ateTimeCreation = ?, AutonomyThreshold = ?, AutonomyThreshold2 = ?, Days = ?, Da" & _
        "ys2 = ? WHERE (KeyWordID = ?) AND (AutonomyThreshold = ? OR ? IS NULL AND Autono" & _
        "myThreshold IS NULL) AND (AutonomyThreshold2 = ? OR ? IS NULL AND AutonomyThresh" & _
        "old2 IS NULL) AND (DateTimeCreation = ? OR ? IS NULL AND DateTimeCreation IS NUL" & _
        "L) AND (Days = ? OR ? IS NULL AND Days IS NULL) AND (Days2 = ? OR ? IS NULL AND " & _
        "Days2 IS NULL) AND (Description = ? OR ? IS NULL AND Description IS NULL) AND (L" & _
        "istNumber = ?) AND (ScanpixID = ? OR ? IS NULL AND ScanpixID IS NULL); SELECT Ke" & _
        "yWordID, ListNumber, Description, KeyWord, ScanpixID, DateTimeCreation, Autonomy" & _
        "Threshold, AutonomyThreshold2, Days, Days2 FROM KeyWord WHERE (KeyWordID = ?)"
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold2"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days", System.Data.OleDb.OleDbType.Integer, 4, "Days"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days2", System.Data.OleDb.OleDbType.Integer, 4, "Days2"))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold21", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation1", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days2", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Days21", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Days2", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID1", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbCommand3.Parameters.Add(New System.Data.OleDb.OleDbParameter("Select_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, "KeyWordID"))
        '
        'OleDbCommand4
        '
        Me.OleDbCommand4.CommandText = "INSERT INTO KeyWord(ListNumber, Description, KeyWord, ScanpixID, DateTimeCreation" & _
        ", AutonomyThreshold, AutonomyThreshold2, Days, Days2) VALUES (?, ?, ?, ?, ?, ?, " & _
        "?, ?, ?); SELECT KeyWordID, ListNumber, Description, KeyWord, ScanpixID, DateTim" & _
        "eCreation, AutonomyThreshold, AutonomyThreshold2, Days, Days2 FROM KeyWord WHERE" & _
        " (KeyWordID = @@IDENTITY)"
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold2", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold2"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days", System.Data.OleDb.OleDbType.Integer, 4, "Days"))
        Me.OleDbCommand4.Parameters.Add(New System.Data.OleDb.OleDbParameter("Days2", System.Data.OleDb.OleDbType.Integer, 4, "Days2"))
        '
        'lblKeyWordID
        '
        Me.lblKeyWordID.Enabled = False
        Me.lblKeyWordID.Location = New System.Drawing.Point(16, 11)
        Me.lblKeyWordID.Name = "lblKeyWordID"
        Me.lblKeyWordID.Size = New System.Drawing.Size(64, 16)
        Me.lblKeyWordID.TabIndex = 35
        Me.lblKeyWordID.Text = "KeyWordID"
        '
        'lblListNumber
        '
        Me.lblListNumber.Location = New System.Drawing.Point(16, 40)
        Me.lblListNumber.Name = "lblListNumber"
        Me.lblListNumber.Size = New System.Drawing.Size(64, 23)
        Me.lblListNumber.TabIndex = 38
        Me.lblListNumber.Text = "Listenr."
        '
        'editScanpixID
        '
        Me.editScanpixID.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.ScanpixID"))
        Me.editScanpixID.Location = New System.Drawing.Point(568, 40)
        Me.editScanpixID.Name = "editScanpixID"
        Me.editScanpixID.Size = New System.Drawing.Size(80, 20)
        Me.editScanpixID.TabIndex = 5
        Me.editScanpixID.Text = ""
        '
        'editDateTimeCreation
        '
        Me.editDateTimeCreation.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.DateTimeCreation"))
        Me.editDateTimeCreation.Enabled = False
        Me.editDateTimeCreation.Location = New System.Drawing.Point(512, 8)
        Me.editDateTimeCreation.Name = "editDateTimeCreation"
        Me.editDateTimeCreation.Size = New System.Drawing.Size(136, 20)
        Me.editDateTimeCreation.TabIndex = 2
        Me.editDateTimeCreation.Text = ""
        '
        'btnNavPrev
        '
        Me.btnNavPrev.Location = New System.Drawing.Point(160, 360)
        Me.btnNavPrev.Name = "btnNavPrev"
        Me.btnNavPrev.Size = New System.Drawing.Size(35, 23)
        Me.btnNavPrev.TabIndex = 14
        Me.btnNavPrev.Text = "<"
        '
        'btnNavNext
        '
        Me.btnNavNext.Location = New System.Drawing.Point(288, 360)
        Me.btnNavNext.Name = "btnNavNext"
        Me.btnNavNext.Size = New System.Drawing.Size(35, 23)
        Me.btnNavNext.TabIndex = 16
        Me.btnNavNext.Text = ">"
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(320, 360)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(40, 23)
        Me.btnLast.TabIndex = 17
        Me.btnLast.Text = ">>"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(120, 392)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.TabIndex = 19
        Me.btnAdd.Text = "&Ny sak"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(200, 392)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TabIndex = 20
        Me.btnDelete.Text = "&Slett"
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(384, 360)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.TabIndex = 18
        Me.btnLoad.Text = "&Load"
        Me.btnLoad.Visible = False
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(176, 40)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(40, 23)
        Me.lblDescription.TabIndex = 40
        Me.lblDescription.Text = "Tittel"
        '
        'lblKeyWord
        '
        Me.lblKeyWord.Location = New System.Drawing.Point(16, 72)
        Me.lblKeyWord.Name = "lblKeyWord"
        Me.lblKeyWord.Size = New System.Drawing.Size(64, 23)
        Me.lblKeyWord.TabIndex = 41
        Me.lblKeyWord.Text = "S�keord"
        '
        'lblScanpixID
        '
        Me.lblScanpixID.Location = New System.Drawing.Point(496, 40)
        Me.lblScanpixID.Name = "lblScanpixID"
        Me.lblScanpixID.Size = New System.Drawing.Size(64, 23)
        Me.lblScanpixID.TabIndex = 43
        Me.lblScanpixID.Text = "ScanpixID"
        '
        'lblDateTimeCreation
        '
        Me.lblDateTimeCreation.Location = New System.Drawing.Point(448, 8)
        Me.lblDateTimeCreation.Name = "lblDateTimeCreation"
        Me.lblDateTimeCreation.Size = New System.Drawing.Size(56, 23)
        Me.lblDateTimeCreation.TabIndex = 45
        Me.lblDateTimeCreation.Text = "Opprettet"
        '
        'editKeyWordID
        '
        Me.editKeyWordID.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.KeyWordID"))
        Me.editKeyWordID.Enabled = False
        Me.editKeyWordID.Location = New System.Drawing.Point(120, 8)
        Me.editKeyWordID.Name = "editKeyWordID"
        Me.editKeyWordID.Size = New System.Drawing.Size(80, 20)
        Me.editKeyWordID.TabIndex = 1
        Me.editKeyWordID.Text = ""
        '
        'editListNumber
        '
        Me.editListNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.ListNumber"))
        Me.editListNumber.Location = New System.Drawing.Point(120, 40)
        Me.editListNumber.Name = "editListNumber"
        Me.editListNumber.Size = New System.Drawing.Size(40, 20)
        Me.editListNumber.TabIndex = 3
        Me.editListNumber.Text = ""
        '
        'editDescription
        '
        Me.editDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.Description"))
        Me.editDescription.Location = New System.Drawing.Point(216, 40)
        Me.editDescription.Name = "editDescription"
        Me.editDescription.Size = New System.Drawing.Size(264, 20)
        Me.editDescription.TabIndex = 4
        Me.editDescription.Text = ""
        '
        'editKeyWord
        '
        Me.editKeyWord.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.KeyWord"))
        Me.editKeyWord.Location = New System.Drawing.Point(120, 72)
        Me.editKeyWord.Multiline = True
        Me.editKeyWord.Name = "editKeyWord"
        Me.editKeyWord.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.editKeyWord.Size = New System.Drawing.Size(528, 128)
        Me.editKeyWord.TabIndex = 6
        Me.editKeyWord.Text = ""
        '
        'btnNavFirst
        '
        Me.btnNavFirst.Location = New System.Drawing.Point(120, 360)
        Me.btnNavFirst.Name = "btnNavFirst"
        Me.btnNavFirst.Size = New System.Drawing.Size(40, 23)
        Me.btnNavFirst.TabIndex = 13
        Me.btnNavFirst.Text = "<<"
        '
        'lblNavLocation
        '
        Me.lblNavLocation.BackColor = System.Drawing.Color.White
        Me.lblNavLocation.Location = New System.Drawing.Point(192, 360)
        Me.lblNavLocation.Name = "lblNavLocation"
        Me.lblNavLocation.Size = New System.Drawing.Size(95, 23)
        Me.lblNavLocation.TabIndex = 15
        Me.lblNavLocation.Text = "No Records"
        Me.lblNavLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(288, 392)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 21
        Me.btnCancel.Text = "Av&bryt"
        '
        'btnAvslutt
        '
        Me.btnAvslutt.Location = New System.Drawing.Point(576, 392)
        Me.btnAvslutt.Name = "btnAvslutt"
        Me.btnAvslutt.TabIndex = 23
        Me.btnAvslutt.Text = "&Avslutt"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 212)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 20)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "Begrens til stoffgrupper:"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(152, 208)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(72, 24)
        Me.CheckBox1.TabIndex = 7
        Me.CheckBox1.Text = "Innenriks"
        '
        'CheckBox2
        '
        Me.CheckBox2.Location = New System.Drawing.Point(240, 208)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(72, 24)
        Me.CheckBox2.TabIndex = 8
        Me.CheckBox2.Text = "Utenriks"
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(320, 208)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(56, 24)
        Me.CheckBox3.TabIndex = 9
        Me.CheckBox3.Text = "Sport"
        '
        'editMaingroup
        '
        Me.editMaingroup.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.Maingroup"))
        Me.editMaingroup.Enabled = False
        Me.editMaingroup.Location = New System.Drawing.Point(512, 210)
        Me.editMaingroup.Name = "editMaingroup"
        Me.editMaingroup.Size = New System.Drawing.Size(136, 20)
        Me.editMaingroup.TabIndex = 10
        Me.editMaingroup.Text = ""
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(384, 213)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 20)
        Me.Label2.TabIndex = 61
        Me.Label2.Text = "(ingen kryss gir alle)"
        '
        'DataForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(664, 429)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label2, Me.editMaingroup, Me.CheckBox3, Me.CheckBox2, Me.CheckBox1, Me.Label1, Me.btnAvslutt, Me.GroupBox2, Me.GroupBox1, Me.btnUpdate, Me.lblKeyWordID, Me.lblListNumber, Me.editScanpixID, Me.editDateTimeCreation, Me.btnNavPrev, Me.btnNavNext, Me.btnLast, Me.btnAdd, Me.btnDelete, Me.btnLoad, Me.lblDescription, Me.lblKeyWord, Me.lblScanpixID, Me.lblDateTimeCreation, Me.editKeyWordID, Me.editListNumber, Me.editDescription, Me.editKeyWord, Me.btnNavFirst, Me.lblNavLocation, Me.btnCancel})
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "DataForm"
        Me.Text = "Aktuelle saker i NTB Portalen"
        CType(Me.objCaseList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.BindingContext(objCaseList, "KeyWord").CancelCurrentEdit()
        Me.objCaseList_PositionChanged()
        LockControls(False)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If (Me.BindingContext(objCaseList, "KeyWord").Count > 0) Then
            Me.BindingContext(objCaseList, "KeyWord").RemoveAt(Me.BindingContext(objCaseList, "KeyWord").Position)
            Me.objCaseList_PositionChanged()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'Clear out the current edits
            Me.BindingContext(objCaseList, "KeyWord").EndCurrentEdit()
            Me.BindingContext(objCaseList, "KeyWord").AddNew()
            Me.editDateTimeCreation.Text = Format(Now, "dd.MM.yyyy HH:mm:ss")
            Me.editAutonomyThreshold.Text = "20"
            Me.editAutonomyThreshold2.Text = "20"
            Me.editDays.Text = "7"
            Me.editDays2.Text = "60"
            Me.editListNumber.Text = "1"
            'Me.editMaingroup.Text = "Alle"
            MakeMainGroup()
        Catch eEndEdit As System.Exception
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
        Me.objCaseList_PositionChanged()
        LockControls(True)
    End Sub

    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        Try
            'Attempt to load the dataset.
            Me.LoadDataSet()
        Catch eLoad As System.Exception
            'Add your error handling code here.
            'Display error message, if any.
            System.Windows.Forms.MessageBox.Show(eLoad.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = 0
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = (Me.objCaseList.Tables("KeyWord").Rows.Count - 1)
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = (Me.BindingContext(objCaseList, "KeyWord").Position - 1)
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = (Me.BindingContext(objCaseList, "KeyWord").Position + 1)
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub objCaseList_PositionChanged()
        Me.lblNavLocation.Text = (((Me.BindingContext(objCaseList, "KeyWord").Position + 1).ToString + " of  ") _
                    + Me.BindingContext(objCaseList, "KeyWord").Count.ToString)

        SetMainGroup()
    End Sub

    Private Sub btnCancelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.objCaseList.RejectChanges()
    End Sub

    Public Sub UpdateDataSet()
        'Create a new dataset to hold the changes that have been made to the main dataset.
        Dim objDataSetChanges As CaseListEdit.CaseList = New CaseListEdit.CaseList()
        'Stop any current edits.
        Me.BindingContext(objCaseList, "KeyWord").EndCurrentEdit()
        'Get the changes that have been made to the main dataset.
        objDataSetChanges = CType(objCaseList.GetChanges, CaseListEdit.CaseList)
        'Check to see if any changes have been made.
        If (Not (objDataSetChanges) Is Nothing) Then
            Try
                'There are changes that need to be made, so attempt to update the datasource by
                'calling the update method and passing the dataset and any parameters.
                Me.UpdateDataSource(objDataSetChanges)
                objCaseList.Merge(objDataSetChanges)
                objCaseList.AcceptChanges()
            Catch eUpdate As System.Exception
                'Add your error handling code here.
                Throw eUpdate
            End Try
            'Add your code to check the returned dataset for any errors that may have been
            'pushed into the row object's error.
        End If
    End Sub

    Public Sub LoadDataSet()
        'Create a new dataset to hold the records returned from the call to FillDataSet.
        'A temporary dataset is used because filling the existing dataset would
        'require the databindings to be rebound.
        Dim objDataSetTemp As CaseListEdit.CaseList
        objDataSetTemp = New CaseListEdit.CaseList()
        Try
            'Attempt to fill the temporary dataset.
            Me.FillDataSet(objDataSetTemp)
        Catch eFillDataSet As System.Exception
            'Add your error handling code here.
            Throw eFillDataSet
        End Try
        Try
            'Empty the old records from the dataset.
            objCaseList.Clear()
            'Merge the records into the main dataset.
            objCaseList.Merge(objDataSetTemp)
        Catch eLoadMerge As System.Exception
            'Add your error handling code here.
            Throw eLoadMerge
        End Try
    End Sub

    Public Sub UpdateDataSource(ByVal ChangedRows As CaseListEdit.CaseList)
        Try
            'The data source only needs to be updated if there are changes pending.
            If (Not (ChangedRows) Is Nothing) Then
                'Open the connection.
                Me.OleDbConnection1.Open()
                'Attempt to update the data source.
                OleDbDataAdapter1.Update(ChangedRows)
            End If
        Catch updateException As System.Exception
            'Add your error handling code here.
            Throw updateException
        Finally
            'Close the connection whether or not the exception was thrown.
            Me.OleDbConnection1.Close()
        End Try
    End Sub

    Public Sub FillDataSet(ByVal dataSet As CaseListEdit.CaseList)
        'Turn off constraint checking before the dataset is filled.
        'This allows the adapters to fill the dataset without concern
        'for dependencies between the tables.
        dataSet.EnforceConstraints = False
        Try
            'Open the connection.
            Me.OleDbConnection1.Open()
            'Attempt to fill the dataset through the OleDbDataAdapter1.
            Me.OleDbDataAdapter1.Fill(dataSet)
        Catch fillException As System.Exception
            'Add your error handling code here.
            Throw fillException
        Finally
            'Turn constraint checking back on.
            dataSet.EnforceConstraints = True
            'Close the connection whether or not the exception was thrown.
            Me.OleDbConnection1.Close()
        End Try
    End Sub


    Private Sub DataForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OleDbConnection1.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("ConnectionString")
        Try
            'Attempt to load the dataset.
            Me.LoadDataSet()
        Catch eLoad As System.Exception
            'Add your error handling code here.
            'Display error message, if any.
            System.Windows.Forms.MessageBox.Show(eLoad.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            'Attempt to update the datasource.
            Me.UpdateDataSet()
            LockControls(False)
        Catch eUpdate As System.Exception
            'Add your error handling code here.
            'Display error message, if any.
            System.Windows.Forms.MessageBox.Show(eUpdate.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Public Sub LockControls(ByVal blnLocked As Boolean)
        blnLocked = Not blnLocked
        Me.btnAdd.Enabled = blnLocked
        Me.btnDelete.Enabled = blnLocked
        Me.btnLast.Enabled = blnLocked
        Me.btnNavFirst.Enabled = blnLocked
        Me.btnNavNext.Enabled = blnLocked
        Me.btnNavPrev.Enabled = blnLocked
    End Sub

    Private Sub editListNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles editListNumber.Leave
        If editListNumber.Text > "3" Or editListNumber.Text < "0" Then
            MsgBox("Verdien m� v�re fra 0-3!")
            editListNumber.Focus()
        End If
    End Sub

    Private Sub editDays_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles editDays.Leave
        If editDays.Text > "7" Or editDays.Text < "1" Then
            MsgBox("Bruk kun 1-7 dager!")
            editDays.Focus()
        End If
    End Sub

    Private Sub btnAvslutt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAvslutt.Click
        Me.Close()
    End Sub

    Private Sub DataForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        'Create a new dataset to hold the changes that have been made to the main dataset.
        Dim objDataSetChanges As CaseListEdit.CaseList = New CaseListEdit.CaseList()

        'Stop any current edits.
        Try
            Me.BindingContext(objCaseList, "KeyWord").EndCurrentEdit()
        Catch err As Exception
            If MsgBox("Du har ikke fylt ut alle felt i ny sak, vil du avbryte registreringen av denne saken?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                e.Cancel = True
                Exit Sub
            End If
        End Try

        'Get the changes that have been made to the main dataset.
        objDataSetChanges = CType(objCaseList.GetChanges, CaseListEdit.CaseList)
        'Check to see if any changes have been made.
        If (objDataSetChanges) Is Nothing Then
            ' No changes made, program is closing
            Exit Sub
        End If

        'If changes is made:
        Dim intResult As Integer
        intResult = MsgBox(MSG_SAVE_BEFORE_CLOSE, MsgBoxStyle.YesNoCancel, "Avslutt")
        If intResult = MsgBoxResult.Cancel Then
            e.Cancel = True
            Return
        ElseIf intResult = MsgBoxResult.Yes Then
            Try
                'Attempt to update the datasource.
                Me.UpdateDataSet()
                LockControls(False)
            Catch eUpdate As System.Exception
                'Display error message, if any.
                System.Windows.Forms.MessageBox.Show(eUpdate.Message)
                e.Cancel = True
                Return
            End Try
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        MakeMainGroup()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        MakeMainGroup()
    End Sub

    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        MakeMainGroup()
    End Sub

    Private Sub MakeMainGroup()
        Dim strMainGroup As String
        Dim btmMainGroup As Long = 0

        If CheckBox1.Checked Then
            'strMainGroup = "Innenriks "
            btmMainGroup = (btmMainGroup Or 1)
        End If
        If CheckBox2.Checked Then
            'strMainGroup &= "Utenriks "
            btmMainGroup = (btmMainGroup Or 2)
        End If
        If CheckBox3.Checked Then
            'strMainGroup &= "Sport "
            btmMainGroup = (btmMainGroup Or 4)
        End If

        If btmMainGroup = 0 Then
            strMainGroup = "Alle"
        Else
            strMainGroup = btmMainGroup
        End If

        Me.editMaingroup.Text = Trim(strMainGroup)

    End Sub

    Private Sub SetMainGroup()
        Dim strMainGroup As String
        Dim btmMainGroup As Long = 0

        strMainGroup = Me.editMaingroup.Text

        Try
            btmMainGroup = strMainGroup
        Catch
            Exit Sub
        End Try

        'Innenriks
        If (btmMainGroup And 1) Then
            CheckBox1.Checked = True
        Else
            CheckBox1.Checked = False
        End If

        'Utenriks
        If (btmMainGroup And 2) Then
            CheckBox2.Checked = True
        Else
            CheckBox2.Checked = False
        End If

        'Sport
        If (btmMainGroup And 4) Then
            CheckBox3.Checked = True
        Else
            CheckBox3.Checked = False
        End If

    End Sub

End Class
