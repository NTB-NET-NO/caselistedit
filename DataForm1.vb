Public Class DataForm1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents OleDbSelectCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbInsertCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbUpdateCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents OleDbDeleteCommand1 As System.Data.OleDb.OleDbCommand
    Friend WithEvents objCaseList As CaseListEdit.CaseList
    Friend WithEvents OleDbConnection1 As System.Data.OleDb.OleDbConnection
    Friend WithEvents OleDbDataAdapter1 As System.Data.OleDb.OleDbDataAdapter
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents btnCancelAll As System.Windows.Forms.Button
    Friend WithEvents lblKeyWord As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblDateTimeCreation As System.Windows.Forms.Label
    Friend WithEvents editKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents editDescription As System.Windows.Forms.TextBox
    Friend WithEvents editDateTimeCreation As System.Windows.Forms.TextBox
    Friend WithEvents lblListNumber As System.Windows.Forms.Label
    Friend WithEvents lblAutonomyThreshold As System.Windows.Forms.Label
    Friend WithEvents lblScanpixID As System.Windows.Forms.Label
    Friend WithEvents editListNumber As System.Windows.Forms.TextBox
    Friend WithEvents editAutonomyThreshold As System.Windows.Forms.TextBox
    Friend WithEvents editScanpixID As System.Windows.Forms.TextBox
    Friend WithEvents btnNavFirst As System.Windows.Forms.Button
    Friend WithEvents btnNavPrev As System.Windows.Forms.Button
    Friend WithEvents lblNavLocation As System.Windows.Forms.Label
    Friend WithEvents btnNavNext As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.OleDbSelectCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbConnection1 = New System.Data.OleDb.OleDbConnection()
        Me.OleDbInsertCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbUpdateCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.OleDbDeleteCommand1 = New System.Data.OleDb.OleDbCommand()
        Me.objCaseList = New CaseListEdit.CaseList()
        Me.OleDbDataAdapter1 = New System.Data.OleDb.OleDbDataAdapter()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnCancelAll = New System.Windows.Forms.Button()
        Me.lblKeyWord = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblDateTimeCreation = New System.Windows.Forms.Label()
        Me.editKeyWord = New System.Windows.Forms.TextBox()
        Me.editDescription = New System.Windows.Forms.TextBox()
        Me.editDateTimeCreation = New System.Windows.Forms.TextBox()
        Me.lblListNumber = New System.Windows.Forms.Label()
        Me.lblAutonomyThreshold = New System.Windows.Forms.Label()
        Me.lblScanpixID = New System.Windows.Forms.Label()
        Me.editListNumber = New System.Windows.Forms.TextBox()
        Me.editAutonomyThreshold = New System.Windows.Forms.TextBox()
        Me.editScanpixID = New System.Windows.Forms.TextBox()
        Me.btnNavFirst = New System.Windows.Forms.Button()
        Me.btnNavPrev = New System.Windows.Forms.Button()
        Me.lblNavLocation = New System.Windows.Forms.Label()
        Me.btnNavNext = New System.Windows.Forms.Button()
        Me.btnLast = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        CType(Me.objCaseList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OleDbSelectCommand1
        '
        Me.OleDbSelectCommand1.CommandText = "SELECT KeyWordID, KeyWord, Description, DateTimeCreation, DateTimeActuality, List" & _
        "Number, AutonomyThreshold, ScanpixID FROM KeyWord"
        Me.OleDbSelectCommand1.Connection = Me.OleDbConnection1
        '
        'OleDbInsertCommand1
        '
        Me.OleDbInsertCommand1.CommandText = "INSERT INTO KeyWord(KeyWord, Description, DateTimeCreation, DateTimeActuality, Li" & _
        "stNumber, AutonomyThreshold, ScanpixID) VALUES (?, ?, ?, ?, ?, ?, ?); SELECT Key" & _
        "WordID, KeyWord, Description, DateTimeCreation, DateTimeActuality, ListNumber, A" & _
        "utonomyThreshold, ScanpixID FROM KeyWord WHERE (KeyWordID = @@IDENTITY)"
        Me.OleDbInsertCommand1.Connection = Me.OleDbConnection1
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeActuality", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeActuality"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbInsertCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        '
        'OleDbUpdateCommand1
        '
        Me.OleDbUpdateCommand1.CommandText = "UPDATE KeyWord SET KeyWord = ?, Description = ?, DateTimeCreation = ?, DateTimeAc" & _
        "tuality = ?, ListNumber = ?, AutonomyThreshold = ?, ScanpixID = ? WHERE (KeyWord" & _
        "ID = ?) AND (AutonomyThreshold = ? OR ? IS NULL AND AutonomyThreshold IS NULL) A" & _
        "ND (DateTimeActuality = ? OR ? IS NULL AND DateTimeActuality IS NULL) AND (DateT" & _
        "imeCreation = ?) AND (Description = ? OR ? IS NULL AND Description IS NULL) AND " & _
        "(ListNumber = ?) AND (ScanpixID = ? OR ? IS NULL AND ScanpixID IS NULL); SELECT " & _
        "KeyWordID, KeyWord, Description, DateTimeCreation, DateTimeActuality, ListNumber" & _
        ", AutonomyThreshold, ScanpixID FROM KeyWord WHERE (KeyWordID = ?)"
        Me.OleDbUpdateCommand1.Connection = Me.OleDbConnection1
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("KeyWord", System.Data.OleDb.OleDbType.VarChar, 2147483647, "KeyWord"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Description", System.Data.OleDb.OleDbType.VarChar, 32, "Description"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeCreation"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("DateTimeActuality", System.Data.OleDb.OleDbType.DBTimeStamp, 8, "DateTimeActuality"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, "ListNumber"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, "AutonomyThreshold"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, "ScanpixID"))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeActuality", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeActuality", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeActuality1", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeActuality", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID1", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbUpdateCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Select_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, "KeyWordID"))
        '
        'OleDbDeleteCommand1
        '
        Me.OleDbDeleteCommand1.CommandText = "DELETE FROM KeyWord WHERE (KeyWordID = ?) AND (AutonomyThreshold = ? OR ? IS NULL" & _
        " AND AutonomyThreshold IS NULL) AND (DateTimeActuality = ? OR ? IS NULL AND Date" & _
        "TimeActuality IS NULL) AND (DateTimeCreation = ?) AND (Description = ? OR ? IS N" & _
        "ULL AND Description IS NULL) AND (ListNumber = ?) AND (ScanpixID = ? OR ? IS NUL" & _
        "L AND ScanpixID IS NULL)"
        Me.OleDbDeleteCommand1.Connection = Me.OleDbConnection1
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_KeyWordID", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "KeyWordID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_AutonomyThreshold1", System.Data.OleDb.OleDbType.Integer, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "AutonomyThreshold", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeActuality", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeActuality", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeActuality1", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeActuality", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_DateTimeCreation", System.Data.OleDb.OleDbType.DBTimeStamp, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "DateTimeCreation", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_Description1", System.Data.OleDb.OleDbType.VarChar, 32, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Description", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ListNumber", System.Data.OleDb.OleDbType.UnsignedTinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ListNumber", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        Me.OleDbDeleteCommand1.Parameters.Add(New System.Data.OleDb.OleDbParameter("Original_ScanpixID1", System.Data.OleDb.OleDbType.VarChar, 16, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ScanpixID", System.Data.DataRowVersion.Original, Nothing))
        '
        'objCaseList
        '
        Me.objCaseList.DataSetName = "CaseList"
        Me.objCaseList.Locale = New System.Globalization.CultureInfo("nb-NO")
        Me.objCaseList.Namespace = "http://www.tempuri.org/CaseList.xsd"
        '
        'OleDbDataAdapter1
        '
        Me.OleDbDataAdapter1.DeleteCommand = Me.OleDbDeleteCommand1
        Me.OleDbDataAdapter1.InsertCommand = Me.OleDbInsertCommand1
        Me.OleDbDataAdapter1.SelectCommand = Me.OleDbSelectCommand1
        Me.OleDbDataAdapter1.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "KeyWord", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("KeyWordID", "KeyWordID"), New System.Data.Common.DataColumnMapping("KeyWord", "KeyWord"), New System.Data.Common.DataColumnMapping("Description", "Description"), New System.Data.Common.DataColumnMapping("DateTimeCreation", "DateTimeCreation"), New System.Data.Common.DataColumnMapping("DateTimeActuality", "DateTimeActuality"), New System.Data.Common.DataColumnMapping("ListNumber", "ListNumber"), New System.Data.Common.DataColumnMapping("AutonomyThreshold", "AutonomyThreshold"), New System.Data.Common.DataColumnMapping("ScanpixID", "ScanpixID")})})
        Me.OleDbDataAdapter1.UpdateCommand = Me.OleDbUpdateCommand1
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(456, 192)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.TabIndex = 11
        Me.btnUpdate.Text = "&Update"
        '
        'btnCancelAll
        '
        Me.btnCancelAll.Location = New System.Drawing.Point(456, 224)
        Me.btnCancelAll.Name = "btnCancelAll"
        Me.btnCancelAll.TabIndex = 15
        Me.btnCancelAll.Text = "Ca&ncel All"
        '
        'lblKeyWord
        '
        Me.lblKeyWord.Location = New System.Drawing.Point(8, 48)
        Me.lblKeyWord.Name = "lblKeyWord"
        Me.lblKeyWord.Size = New System.Drawing.Size(72, 23)
        Me.lblKeyWord.TabIndex = 4
        Me.lblKeyWord.Text = "S�keord"
        '
        'lblDescription
        '
        Me.lblDescription.Location = New System.Drawing.Point(152, 16)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(64, 23)
        Me.lblDescription.TabIndex = 5
        Me.lblDescription.Text = "Tekst"
        '
        'lblDateTimeCreation
        '
        Me.lblDateTimeCreation.Location = New System.Drawing.Point(8, 144)
        Me.lblDateTimeCreation.Name = "lblDateTimeCreation"
        Me.lblDateTimeCreation.Size = New System.Drawing.Size(56, 23)
        Me.lblDateTimeCreation.TabIndex = 6
        Me.lblDateTimeCreation.Text = "Opprettet"
        '
        'editKeyWord
        '
        Me.editKeyWord.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.KeyWord"))
        Me.editKeyWord.Location = New System.Drawing.Point(88, 48)
        Me.editKeyWord.Multiline = True
        Me.editKeyWord.Name = "editKeyWord"
        Me.editKeyWord.Size = New System.Drawing.Size(440, 56)
        Me.editKeyWord.TabIndex = 2
        Me.editKeyWord.Text = ""
        '
        'editDescription
        '
        Me.editDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.Description"))
        Me.editDescription.Location = New System.Drawing.Point(224, 16)
        Me.editDescription.Name = "editDescription"
        Me.editDescription.Size = New System.Drawing.Size(304, 20)
        Me.editDescription.TabIndex = 1
        Me.editDescription.Text = ""
        '
        'editDateTimeCreation
        '
        Me.editDateTimeCreation.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.DateTimeCreation"))
        Me.editDateTimeCreation.Enabled = False
        Me.editDateTimeCreation.Location = New System.Drawing.Point(88, 144)
        Me.editDateTimeCreation.Name = "editDateTimeCreation"
        Me.editDateTimeCreation.Size = New System.Drawing.Size(176, 20)
        Me.editDateTimeCreation.TabIndex = 5
        Me.editDateTimeCreation.Text = ""
        '
        'lblListNumber
        '
        Me.lblListNumber.Location = New System.Drawing.Point(8, 16)
        Me.lblListNumber.Name = "lblListNumber"
        Me.lblListNumber.Size = New System.Drawing.Size(64, 23)
        Me.lblListNumber.TabIndex = 12
        Me.lblListNumber.Text = "Listenr."
        '
        'lblAutonomyThreshold
        '
        Me.lblAutonomyThreshold.Location = New System.Drawing.Point(320, 112)
        Me.lblAutonomyThreshold.Name = "lblAutonomyThreshold"
        Me.lblAutonomyThreshold.Size = New System.Drawing.Size(96, 23)
        Me.lblAutonomyThreshold.TabIndex = 13
        Me.lblAutonomyThreshold.Text = "Autonomy Utplukk"
        '
        'lblScanpixID
        '
        Me.lblScanpixID.Location = New System.Drawing.Point(8, 112)
        Me.lblScanpixID.Name = "lblScanpixID"
        Me.lblScanpixID.Size = New System.Drawing.Size(64, 23)
        Me.lblScanpixID.TabIndex = 14
        Me.lblScanpixID.Text = "ScanpixID"
        '
        'editListNumber
        '
        Me.editListNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.ListNumber"))
        Me.editListNumber.Location = New System.Drawing.Point(88, 16)
        Me.editListNumber.Name = "editListNumber"
        Me.editListNumber.Size = New System.Drawing.Size(48, 20)
        Me.editListNumber.TabIndex = 0
        Me.editListNumber.Text = ""
        '
        'editAutonomyThreshold
        '
        Me.editAutonomyThreshold.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.AutonomyThreshold"))
        Me.editAutonomyThreshold.Location = New System.Drawing.Point(440, 112)
        Me.editAutonomyThreshold.Name = "editAutonomyThreshold"
        Me.editAutonomyThreshold.Size = New System.Drawing.Size(88, 20)
        Me.editAutonomyThreshold.TabIndex = 4
        Me.editAutonomyThreshold.Text = ""
        '
        'editScanpixID
        '
        Me.editScanpixID.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.objCaseList, "KeyWord.ScanpixID"))
        Me.editScanpixID.Location = New System.Drawing.Point(88, 112)
        Me.editScanpixID.Name = "editScanpixID"
        Me.editScanpixID.Size = New System.Drawing.Size(72, 20)
        Me.editScanpixID.TabIndex = 3
        Me.editScanpixID.Text = ""
        '
        'btnNavFirst
        '
        Me.btnNavFirst.Location = New System.Drawing.Point(16, 192)
        Me.btnNavFirst.Name = "btnNavFirst"
        Me.btnNavFirst.Size = New System.Drawing.Size(40, 23)
        Me.btnNavFirst.TabIndex = 6
        Me.btnNavFirst.Text = "<<"
        '
        'btnNavPrev
        '
        Me.btnNavPrev.Location = New System.Drawing.Point(56, 192)
        Me.btnNavPrev.Name = "btnNavPrev"
        Me.btnNavPrev.Size = New System.Drawing.Size(35, 23)
        Me.btnNavPrev.TabIndex = 7
        Me.btnNavPrev.Text = "<"
        '
        'lblNavLocation
        '
        Me.lblNavLocation.BackColor = System.Drawing.Color.White
        Me.lblNavLocation.Location = New System.Drawing.Point(88, 192)
        Me.lblNavLocation.Name = "lblNavLocation"
        Me.lblNavLocation.Size = New System.Drawing.Size(95, 23)
        Me.lblNavLocation.TabIndex = 8
        Me.lblNavLocation.Text = "No Records"
        Me.lblNavLocation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnNavNext
        '
        Me.btnNavNext.Location = New System.Drawing.Point(184, 192)
        Me.btnNavNext.Name = "btnNavNext"
        Me.btnNavNext.Size = New System.Drawing.Size(35, 23)
        Me.btnNavNext.TabIndex = 9
        Me.btnNavNext.Text = ">"
        '
        'btnLast
        '
        Me.btnLast.Location = New System.Drawing.Point(224, 192)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(40, 23)
        Me.btnLast.TabIndex = 10
        Me.btnLast.Text = ">>"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(16, 224)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.TabIndex = 12
        Me.btnAdd.Text = "&Add"
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(104, 224)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.TabIndex = 13
        Me.btnDelete.Text = "&Delete"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(184, 224)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.TabIndex = 14
        Me.btnCancel.Text = "&Cancel"
        '
        'DataForm1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(560, 261)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.btnUpdate, Me.btnCancelAll, Me.lblKeyWord, Me.lblDescription, Me.lblDateTimeCreation, Me.editKeyWord, Me.editDescription, Me.editDateTimeCreation, Me.lblListNumber, Me.lblAutonomyThreshold, Me.lblScanpixID, Me.editListNumber, Me.editAutonomyThreshold, Me.editScanpixID, Me.btnNavFirst, Me.btnNavPrev, Me.lblNavLocation, Me.btnNavNext, Me.btnLast, Me.btnAdd, Me.btnDelete, Me.btnCancel})
        Me.Name = "DataForm1"
        Me.Text = "DataForm1"
        CType(Me.objCaseList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.BindingContext(objCaseList, "KeyWord").CancelCurrentEdit()
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If (Me.BindingContext(objCaseList, "KeyWord").Count > 0) Then
            Me.BindingContext(objCaseList, "KeyWord").RemoveAt(Me.BindingContext(objCaseList, "KeyWord").Position)
            Me.objCaseList_PositionChanged()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            'Clear out the current edits
            Me.BindingContext(objCaseList, "KeyWord").EndCurrentEdit()
            Me.BindingContext(objCaseList, "KeyWord").AddNew()
            Me.editDateTimeCreation.Text = Format(Now, "dd.MM.yyyy HH:mm:ss")
        Catch eEndEdit As System.Exception
            System.Windows.Forms.MessageBox.Show(eEndEdit.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            'Attempt to update the datasource.
            Me.UpdateDataSet()
        Catch eUpdate As System.Exception
            'Add your error handling code here.
            'Display error message, if any.
            System.Windows.Forms.MessageBox.Show(eUpdate.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = 0
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = (Me.objCaseList.Tables("KeyWord").Rows.Count - 1)
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
        Try
            Me.BindingContext(objCaseList, "KeyWord").Position = (Me.BindingContext(objCaseList, "KeyWord").Position - 1)
            Me.objCaseList_PositionChanged()
        Catch err As Exception
            MsgBox(err.Message)
        End Try
    End Sub

    Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
        Me.BindingContext(objCaseList, "KeyWord").Position = (Me.BindingContext(objCaseList, "KeyWord").Position + 1)
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub objCaseList_PositionChanged()
        Me.lblNavLocation.Text = (((Me.BindingContext(objCaseList, "KeyWord").Position + 1).ToString + " of  ") _
                    + Me.BindingContext(objCaseList, "KeyWord").Count.ToString)
    End Sub

    Private Sub btnCancelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAll.Click
        Me.objCaseList.RejectChanges()
    End Sub

    Public Sub UpdateDataSet()
        'Create a new dataset to hold the changes that have been made to the main dataset.
        Dim objDataSetChanges As CaseListEdit.CaseList = New CaseListEdit.CaseList()
        'Stop any current edits.
        Me.BindingContext(objCaseList, "KeyWord").EndCurrentEdit()
        'Get the changes that have been made to the main dataset.
        objDataSetChanges = CType(objCaseList.GetChanges, CaseListEdit.CaseList)
        'Check to see if any changes have been made.
        If (Not (objDataSetChanges) Is Nothing) Then
            Try
                'There are changes that need to be made, so attempt to update the datasource by
                'calling the update method and passing the dataset and any parameters.
                Me.UpdateDataSource(objDataSetChanges)
                objCaseList.Merge(objDataSetChanges)
                objCaseList.AcceptChanges()
            Catch eUpdate As System.Exception
                'Add your error handling code here.
                Throw eUpdate
            End Try
            'Add your code to check the returned dataset for any errors that may have been
            'pushed into the row object's error.
        End If

    End Sub

    Public Sub LoadDataSet()
        'Create a new dataset to hold the records returned from the call to FillDataSet.
        'A temporary dataset is used because filling the existing dataset would
        'require the databindings to be rebound.
        Dim objDataSetTemp As CaseListEdit.CaseList
        objDataSetTemp = New CaseListEdit.CaseList()
        Try
            'Attempt to fill the temporary dataset.
            Me.FillDataSet(objDataSetTemp)
        Catch eFillDataSet As System.Exception
            'Add your error handling code here.
            Throw eFillDataSet
        End Try
        Try
            'Empty the old records from the dataset.
            objCaseList.Clear()
            'Merge the records into the main dataset.
            objCaseList.Merge(objDataSetTemp)
        Catch eLoadMerge As System.Exception
            'Add your error handling code here.
            Throw eLoadMerge
        End Try
    End Sub

    Public Sub UpdateDataSource(ByVal ChangedRows As CaseListEdit.CaseList)
        Try
            'The data source only needs to be updated if there are changes pending.
            If (Not (ChangedRows) Is Nothing) Then
                'Open the connection.
                Me.OleDbConnection1.Open()
                'Attempt to update the data source.
                OleDbDataAdapter1.Update(ChangedRows)
            End If
        Catch updateException As System.Exception
            'Add your error handling code here.
            Throw updateException
        Finally
            'Close the connection whether or not the exception was thrown.
            Me.OleDbConnection1.Close()
        End Try

    End Sub

    Public Sub FillDataSet(ByVal dataSet As CaseListEdit.CaseList)
        'Turn off constraint checking before the dataset is filled.
        'This allows the adapters to fill the dataset without concern
        'for dependencies between the tables.
        dataSet.EnforceConstraints = False
        Try
            'Open the connection.
            Me.OleDbConnection1.Open()
            'Attempt to fill the dataset through the OleDbDataAdapter1.
            Me.OleDbDataAdapter1.Fill(dataSet)
        Catch fillException As System.Exception
            'Add your error handling code here.
            Throw fillException
        Finally
            'Turn constraint checking back on.
            dataSet.EnforceConstraints = True
            'Close the connection whether or not the exception was thrown.
            Me.OleDbConnection1.Close()
        End Try

    End Sub

    Private Sub DataForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OleDbConnection1.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings("ConnectionString")
        Try
            'Attempt to load the dataset.
            Me.LoadDataSet()
        Catch eLoad As System.Exception
            'Add your error handling code here.
            'Display error message, if any.
            System.Windows.Forms.MessageBox.Show(eLoad.Message)
        End Try
        Me.objCaseList_PositionChanged()
    End Sub

    Private Sub btnLoad_Click_old(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    'Attempt to load the dataset.
        '    Me.LoadDataSet()
        'Catch eLoad As System.Exception
        '    'Add your error handling code here.
        '    'Display error message, if any.
        '    System.Windows.Forms.MessageBox.Show(eLoad.Message)
        'End Try
        'Me.objCaseList_PositionChanged()
    End Sub

End Class
